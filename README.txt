== Cluster restart ==

Options for inventory files:
* Development: /etc/ansible/Dev.aws_ec2.yaml
* Quality assurance: /etc/ansible/Dev.aws_ec2.yaml
* Production: /etc/ansible/Dev.aws_ec2.yaml

=== Check if you have access to all relevant instances ===

ansible-playbook -i /etc/ansible/Dev.aws_ec2.yaml /opt/ansible-elvis-restart-cluster/test-access.yaml

Expected output (runtime is 15-20 seconds):

PLAY [Test ansible access to WoodWing Assets cluster] *************************************************************************************************************************************************************************

TASK [Get hostname as root and assign to variable] ****************************************************************************************************************************************************************************
ok: [10.65.158.151]
[..]
ok: [10.65.107.77]

TASK [Report hostname] ********************************************************************************************************************************************************************************************************
ok: [10.65.111.159] => {
    "msg": "ip-10-65-111-159.mediait.ch"
}
[..]
ok: [10.65.121.40] => {
    "msg": "ip-10-65-121-40.mediait.ch"
}

TASK [Check for python and package versions] **********************************************************************************************************************************************************************************
ok: [10.65.158.151]
[..]
ok: [10.65.111.159]

TASK [Check getting EC2 instance metadata facts] ******************************************************************************************************************************************************************************
ok: [10.65.158.151]
[..]
ok: [10.65.107.77]

TASK [Check getting EC2 instance facts] ***************************************************************************************************************************************************************************************
ok: [10.65.158.151]
[..]
ok: [10.65.86.164]

PLAY RECAP ********************************************************************************************************************************************************************************************************************
10.65.107.77               : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
[..]
10.65.90.46                : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

If errors are reported, use "kinit adm-<username>@example.com" and provide your password to ensure that you have a valid
SSO token.

=== Actual cluster restart

ansible-playbook -i /etc/ansible/Dev.aws_ec2.yaml /opt/ansible-elvis-restart-cluster/elvis-restart-cluster.yaml

Expected play recap (details may differ)

PLAY RECAP ***********************************************************************************************************************
10.65.107.183              : ok=16   changed=9    unreachable=0    failed=0    skipped=4    rescued=0    ignored=1
10.65.107.54               : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.115.80               : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.116.13               : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.129.157              : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.139.225              : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.149.74               : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.159.221              : ok=16   changed=9    unreachable=0    failed=0    skipped=4    rescued=0    ignored=1
10.65.79.83                : ok=12   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1
10.65.92.88                : ok=16   changed=9    unreachable=0    failed=0    skipped=4    rescued=0    ignored=1
10.65.97.199               : ok=15   changed=5    unreachable=0    failed=0    skipped=8    rescued=0    ignored=1

Especially no errors are expected. Full logs are available in /var/log/ansible.log
